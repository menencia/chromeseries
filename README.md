ChromeSeries
============

> Extension Google Chrome pour BetaSeries.

Fonctionnalités
---------------

* Connexion, inscription, déconnexion à un compte BetaSeries
* Vues : planning, épisodes non-vus, événements des amis, notifications, compte, recherche de séries ou de membres, fiche d'épisode, fiche de série, commentaires d'un épisode, options
* Marquer comme vu un ou plusieurs épisodes
* Marquer comme récupéré ou pas un épisode
* Télécharger le meilleur sous-titre actuel (VF et/ou VO)
* Noter un épisode
* Ajouter/retirer un ami
* Mode déconnecté
* Langages : français, anglais

Dernière version
----------------

### Version 0.8
* Amélioration considérable du chargement des vues
* Meilleure gestion des données en cache
* Possibilité de supprimer le cache utilisé par l'extension
* Nouveau design général du popup (avec une hauteur fixe)
* Nouvelle mise en page de la vue des épisodes non vus
* Nouvelles mises en page des vus d'une série et d'un épisode
* Meilleure gestion de l'ajout/retrait d'une série/ami
* Ajout de la vue de tous les épisodes d'une série
* Possibilité de revenir à un ancien épisode vu
* Déconnexion forcée lorsqu'une nouvelle mise à jour est détectée

Liens
-----

* [Télécharger l'extension ChromeSeries](https://chrome.google.com/webstore/detail/dadaekemlgdonlfgmfmjnpbgdplffpda)
* [Bugtraker](https://www.betaseries.com/bugs/chromeseries)
* [Page Facebook](http://www.facebook.com/pages/ChromeSeries/199020100116357)
* [Site Web de l'auteur](http://www.menencia.com)
